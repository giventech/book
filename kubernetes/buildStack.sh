docker build -t book-api  .. --build-arg REPOSITORY_URL=docker.io  --build-arg SPRING_PROFILE=dev
docker tag book-api localhost:5000/book-api
docker push localhost:5000/book-api
kubectl apply -f .