package com.reece.address.book.builder;
import com.reece.address.book.model.ContactDTO;


public class ContactDTOBuilder {

    private final ContactDTO contact;

    public ContactDTOBuilder() {
        this.contact = new ContactDTO();

    }
    public ContactDTO build() {return contact;}

    public ContactDTOBuilder withName(String name) {
        this.contact.setName(name);
        return this;
    }

    public ContactDTOBuilder withPhoneNumber(String phoneNumber) {
        this.contact.setPhoneNumber(phoneNumber);
        return this;
    }
    public ContactDTOBuilder withAddressBookId(Long addressBookId) {
        this.contact.setAddressBookId(addressBookId);
        return this;
    }

}
