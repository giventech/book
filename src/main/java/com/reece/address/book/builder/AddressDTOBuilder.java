package com.reece.address.book.builder;

import com.reece.address.book.model.AddressBookDTO;



public class AddressDTOBuilder {

    private final AddressBookDTO addressBook;

    public AddressDTOBuilder() {
        this.addressBook = new AddressBookDTO();

    }
    public AddressBookDTO build() {return addressBook;}

    public AddressDTOBuilder withOwnerId(Long ownerId) {
        this.addressBook.setOwnerId(ownerId);
        return this;
    }

    public AddressDTOBuilder withName(String name) {
        this.addressBook.setName(name);
        return this;
    }

}
