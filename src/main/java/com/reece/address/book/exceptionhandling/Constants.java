package com.reece.address.book.exceptionhandling;

public class Constants {

    // Reasons
    public static final String OWNER_NOT_FOUND = "owner was not found";
    public static final String ADDRESS_BOOK_NOT_FOUND = "Address book not found for owner";
    private Constants() {
    }
}


