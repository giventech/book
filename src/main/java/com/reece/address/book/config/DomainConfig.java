package com.reece.address.book.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@Configuration
@EntityScan("com.reece.address.book.domain")
@EnableJpaRepositories("com.reece.address.book.repos")
@EnableTransactionManagement
public class DomainConfig {
}
