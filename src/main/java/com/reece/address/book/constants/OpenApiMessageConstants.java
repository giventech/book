package com.reece.address.book.constants;


public class OpenApiMessageConstants {

    // Cannot instantiate this class.
    private OpenApiMessageConstants() {
    }

    public static final String CONTACT_CONTROLLER_GET_ADDRESS_BOOK_CONTACT_FOR_USER = "Get an address book contact list for a given  user";
    public static final String CONTACT_CONTROLLER_GET_ADDRESS_BOOKS_CONTACTS = "Get all contacts for 1 to many specified addressBookId  (User should be able to print all contacts in an address book.)";
    public static final String CONTACT_CONTROLLER_GET_ADDRESS_BOOKS_CONTACTS_FOR_USER = "Get all contacts for 1 to many specified addressBookId for a specific user (User should be able to print all contacts in an address book.)";
    public static final String CONTACT_CONTROLLER_DELETE_CONTACT = "Delete contact based on its id, (User is able to remove existing contact entries)";
    public static final String CONTACT_CONTROLLER_CREATE_CONTACT = "Create a new contact (User is able to add new contact entry)";
    public static final String CONTACT_CONTROLLER_CREATE_ADDRESS_BOOKS = "Create a new address book for an owner (User should be able to maintain multiple address books)";


    // Address DTO

    public static final String ADDRESS_BOOK_NAME = "The name of the required address book";

    public static final String TELEPHONE_DESCRIPTION = "Telephone entry description";
    public static final String OWNER_ID_DESCRIPTION = "Must be an existing owner id";
    public static final String ADDRESS_ID_DESCRIPTION = "Must be an existing address id";

    // Contact DTO
    public static final String CONTACT_NAME_DESCRIPTION = "The name the contact";

    // Owner DTO
    public static final String OWNER_NAME_DESCRIPTION = "The name for the owner";
    public static final String ROLE_DESCRIPTION = "Role for the book owner - manager, lead, or consultant";

}
