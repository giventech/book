package com.reece.address.book.service;
import com.reece.address.book.exceptionhandling.Constants;
import com.reece.address.book.repos.OwnerRepository;
import com.reece.address.book.domain.AddressBook;
import com.reece.address.book.domain.Owner;
import com.reece.address.book.model.AddressBookDTO;
import com.reece.address.book.repos.AddressBookRepository;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import javax.transaction.Transactional;
@Service
public class AddressBookService {
    private final AddressBookRepository addressBookRepository;
    private final OwnerRepository ownerRepository;
    public AddressBookService(final AddressBookRepository addressBookRepository,
                              final OwnerRepository ownerRepository) {
        this.addressBookRepository = addressBookRepository;
        this.ownerRepository = ownerRepository;
    }
    public List<AddressBookDTO> findAll() {
        return addressBookRepository.findAll()
                .stream()
                .map(adressBook -> mapToDTO(adressBook, new AddressBookDTO()))
                .collect(Collectors.toList());
    }
    public AddressBookDTO get(final Long id) {
        return addressBookRepository.findById(id)
                .map(adressBook -> mapToDTO(adressBook, new AddressBookDTO()))
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }
    @Transactional
    public Long create(final AddressBookDTO addressBookDTO) {
        final var owner = ownerRepository.findById(addressBookDTO.getOwnerId())
                .orElseThrow(() -> new  ResponseStatusException(HttpStatus.NOT_FOUND, Constants.OWNER_NOT_FOUND));
        final AddressBook addressBook = new AddressBook();
        mapToEntity(addressBookDTO, addressBook);
        return addressBookRepository.save(addressBook).getId();
    }
    public void update(final Long id, final AddressBookDTO addressBookDTO) {
        final AddressBook addressBook = addressBookRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        mapToEntity(addressBookDTO, addressBook);
        addressBookRepository.save(addressBook);
    }
    public void delete(final Long id) {
        addressBookRepository.deleteById(id);
    }
    private AddressBookDTO mapToDTO(final AddressBook addressBook, final AddressBookDTO addressBookDTO) {
        addressBookDTO.setName(addressBook.getName());
        addressBookDTO.setOwnerId(addressBook.getOwner() == null ? null : addressBook.getOwner().getId());
        return addressBookDTO;
    }
    private AddressBook mapToEntity(final AddressBookDTO addressBookDTO, final AddressBook addressBook) {
        addressBook.setName(addressBookDTO.getName());
        if (addressBookDTO.getOwnerId() != null && (addressBook.getOwner() == null || !addressBook.getOwner().getId().equals(addressBookDTO.getOwnerId()))) {
            final Owner owner = ownerRepository.findById(addressBookDTO.getOwnerId())
                    .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "owner not found"));
            addressBook.setOwner(owner);
        }
        return addressBook;
    }
    private void validateOwnerAndAddressBook(Long ownerId, Long addressId) throws ResponseStatusException {
        final var owner = ownerRepository.findById(ownerId)
                .orElseThrow(() -> new  ResponseStatusException(HttpStatus.NOT_FOUND, Constants.OWNER_NOT_FOUND));
        final var addressBook = addressBookRepository
                .findAddressBookByOwnerAndId(owner,addressId);
        if (addressBook.isEmpty()) {
            throw new  ResponseStatusException(HttpStatus.NOT_FOUND,Constants.ADDRESS_BOOK_NOT_FOUND);
        }
    }
}