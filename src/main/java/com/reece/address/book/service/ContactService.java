package com.reece.address.book.service;

import com.reece.address.book.domain.AddressBook;
import com.reece.address.book.domain.Contact;
import com.reece.address.book.exceptionhandling.Constants;
import com.reece.address.book.model.ContactDTO;
import com.reece.address.book.repos.AddressBookRepository;
import com.reece.address.book.repos.ContactRepository;
import java.util.List;
import java.util.stream.Collectors;

import com.reece.address.book.repos.OwnerRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.server.ResponseStatusException;



@Service
public class ContactService {

    private final ContactRepository contactRepository;
    private final AddressBookRepository addressBookRepository;
    private final OwnerRepository ownerRepository;


    public ContactService(final ContactRepository contactRepository,
                          final AddressBookRepository addressBookRepository, OwnerRepository ownerRepository) {
        this.contactRepository = contactRepository;
        this.addressBookRepository = addressBookRepository;
        this.ownerRepository = ownerRepository;
    }

    public List<ContactDTO> findAll() {
        return contactRepository.findAll()
                .stream()
                .map(contact -> mapToDTO(contact, new ContactDTO()))
                .collect(Collectors.toList());
    }

    public List<ContactDTO> getContactsByUserAddressBook(Long userId, @PathVariable final Long addressBookId) {
        return contactRepository.fetchContactsByAddressContactIdAndOwnerId(addressBookId, userId)
                .stream()
                .map(contact -> mapToDTO(contact, new ContactDTO()))
                .collect(Collectors.toList());
    }


    public List<ContactDTO> getContactsFromBooks(@PathVariable final List<Long> addressBookIds) {
        return contactRepository.fetchContactsByAddressBooks(addressBookIds)
                .stream()
                .map(contact -> mapToDTO(contact, new ContactDTO()))
                .collect(Collectors.toList());
    }

    public List<ContactDTO> getAllUserBookContactsFromBookIds(Long userId, @PathVariable final List<Long> addressBookIds) {
        return contactRepository.fetchContactsByAddressContactIdsAndOwnerId(addressBookIds, userId)
                .stream()
                .map(contact -> mapToDTO(contact, new ContactDTO()))
                .collect(Collectors.toList());
    }


    public ContactDTO get(final Long id) {
        return contactRepository.findById(id)
                .map(contact -> mapToDTO(contact, new ContactDTO()))
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    public Long create(final Long ownerId, final ContactDTO contactDTO) {
        final var owner = ownerRepository.findById(ownerId)
         .orElseThrow(() -> new  ResponseStatusException(HttpStatus.NOT_FOUND,Constants.OWNER_NOT_FOUND));

        final var addressBook = addressBookRepository
                .findAddressBookByOwnerAndId(owner,contactDTO.getAddressBookId());

        if (addressBook.isEmpty()) {
            throw new  ResponseStatusException(HttpStatus.NOT_FOUND,Constants.ADDRESS_BOOK_NOT_FOUND);
        }

        final Contact contact = new Contact();
        mapToEntity(contactDTO, contact);
        return contactRepository.save(contact).getId();
    }

    public void update(final Long id, final ContactDTO contactDTO) {
        final Contact contact = contactRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        mapToEntity(contactDTO, contact);
        contactRepository.save(contact);
    }

    public void delete(final Long id) {
        contactRepository.deleteById(id);
    }

    private ContactDTO mapToDTO(final Contact contact, final ContactDTO contactDTO) {
        contactDTO.setName(contact.getName());
        contactDTO.setPhoneNumber(contact.getPhoneNumber());
        contactDTO.setAddressBookId(contact.getAddressContact() == null ? null : contact.getAddressContact().getId());
        return contactDTO;
    }

    private Contact mapToEntity(final ContactDTO contactDTO, final Contact contact) {
        contact.setName(contactDTO.getName());
        contact.setPhoneNumber(contactDTO.getPhoneNumber());
        if (contactDTO.getAddressBookId() != null && (contact.getAddressContact() == null || !contact.getAddressContact().getId().equals(contactDTO.getAddressBookId()))) {
            final AddressBook addressContact = addressBookRepository.findById(contactDTO.getAddressBookId())
                    .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "addressContact not found"));
            contact.setAddressContact(addressContact);
        }
        return contact;
    }

    private void validateOwnerAndAddressBook(Long ownerId, Long addressId) throws ResponseStatusException {
        final var owner = ownerRepository.findById(ownerId)
                .orElseThrow(() -> new  ResponseStatusException(HttpStatus.NOT_FOUND,Constants.OWNER_NOT_FOUND));

        final var addressBook = addressBookRepository
                .findAddressBookByOwnerAndId(owner,addressId);

        if (addressBook.isEmpty()) {
            throw new  ResponseStatusException(HttpStatus.NOT_FOUND,Constants.ADDRESS_BOOK_NOT_FOUND);
        }

    }

}
