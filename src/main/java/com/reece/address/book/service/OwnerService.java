package com.reece.address.book.service;

import com.reece.address.book.domain.Owner;
import com.reece.address.book.repos.OwnerRepository;
import com.reece.address.book.model.OwnerDTO;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;


@Service
public class OwnerService {

    private final OwnerRepository ownerRepository;

    public OwnerService(final OwnerRepository ownerRepository) {
        this.ownerRepository = ownerRepository;
    }

    public List<OwnerDTO> findAll() {
        return ownerRepository.findAll()
                .stream()
                .map(owner -> mapToDTO(owner, new OwnerDTO()))
                .collect(Collectors.toList());
    }

    public OwnerDTO get(final Long id) {
        return ownerRepository.findById(id)
                .map(owner -> mapToDTO(owner, new OwnerDTO()))
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    public Long create(final OwnerDTO ownerDTO) {
        final Owner owner = new Owner();
        mapToEntity(ownerDTO, owner);
        return ownerRepository.save(owner).getId();
    }

    public void update(final Long id, final OwnerDTO ownerDTO) {
        final Owner owner = ownerRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        mapToEntity(ownerDTO, owner);
        ownerRepository.save(owner);
    }

    public void delete(final Long id) {
        ownerRepository.deleteById(id);
    }

    private OwnerDTO mapToDTO(final Owner owner, final OwnerDTO ownerDTO) {
        ownerDTO.setName(owner.getName());
        ownerDTO.setRole(owner.getRole());
        return ownerDTO;
    }

    private Owner mapToEntity(final OwnerDTO ownerDTO, final Owner owner) {
        owner.setName(ownerDTO.getName());
        owner.setRole(ownerDTO.getRole());
        return owner;
    }

}
