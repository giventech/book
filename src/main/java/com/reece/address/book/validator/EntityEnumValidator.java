package com.reece.address.book.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class EntityEnumValidator implements ConstraintValidator<EntityEnum, CharSequence> {

    private List<String> acceptedValues;
    private boolean allowEmptyValue;
    private boolean caseInSensitive;

    @Override
    public void initialize(EntityEnum annotation) {

        caseInSensitive = annotation.caseInSensitive();
        acceptedValues = Stream.of(annotation.enumClass().getEnumConstants())
                .map(e -> e.toString().toLowerCase())
                .collect(Collectors.toList());
        allowEmptyValue = annotation.allowEmptyValue();
    }

    @Override
    public boolean isValid(CharSequence value, ConstraintValidatorContext context) {
        if (allowEmptyValue && value == null) {
            return true;
        }

        if (!allowEmptyValue && value == null) {
            return false;
        }

        if(caseInSensitive){
            return acceptedValues.contains(value.toString().toLowerCase());
        }

        return acceptedValues.contains(value.toString());
    }
}
