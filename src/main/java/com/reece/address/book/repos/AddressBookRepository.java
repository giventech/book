package com.reece.address.book.repos;

import com.reece.address.book.domain.AddressBook;
import com.reece.address.book.domain.Owner;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface AddressBookRepository extends JpaRepository<AddressBook, Long> {
    Optional<AddressBook> findAddressBookByOwnerAndId(Owner owner, Long Id);
}
