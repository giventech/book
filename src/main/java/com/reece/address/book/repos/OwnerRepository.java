package com.reece.address.book.repos;

import com.reece.address.book.domain.Owner;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;


public interface OwnerRepository extends JpaRepository<Owner, Long> {
}
