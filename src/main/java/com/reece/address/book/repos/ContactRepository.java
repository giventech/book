package com.reece.address.book.repos;

import com.reece.address.book.domain.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface ContactRepository extends JpaRepository<Contact, Long> {


    // Query below uses join rather than join fetch to lazy load Contacts
    @Query(value = "select contacts FROM Contact contacts INNER JOIN contacts.addressContact addresses   where addresses.id IN (:addressIds)")
    List<Contact> fetchContactsByAddressBooks(@Param("addressIds") List<Long> addressIds);

    // Query below uses join rather than join fetch to lazy load Contacts
    @Query(value = "select contacts FROM Contact contacts INNER JOIN contacts.addressContact addresses INNER JOIN addresses.owner owner  where addresses.id = :addressId and owner.id = :ownerId")
    List<Contact> fetchContactsByAddressContactIdAndOwnerId(@Param("addressId") Long addressId, @Param("ownerId")  Long ownerId);

    // Query below uses join rather than join fetch to lazy load Contacts
    @Query(value = "select contacts FROM Contact contacts INNER JOIN contacts.addressContact addresses INNER JOIN addresses.owner owner  where addresses.id IN (:addressIds) and owner.id = :ownerId")
    List<Contact> fetchContactsByAddressContactIdsAndOwnerId(@Param("addressIds") List<Long> addressIds, @Param("ownerId")  Long ownerId);

}
