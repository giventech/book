package com.reece.address.book.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import static com.reece.address.book.constants.OpenApiMessageConstants.ADDRESS_BOOK_NAME;
import static com.reece.address.book.constants.OpenApiMessageConstants.OWNER_ID_DESCRIPTION;


@Getter
@Setter
public class AddressBookDTO {

    @Size(max = 255)
    @NotEmpty (message = "Please provide address book name")
    @Schema(description = ADDRESS_BOOK_NAME, example = "Work related address book")

    private String name;

    @NotNull(message = "Please enter ownerId")
    @Schema(description = OWNER_ID_DESCRIPTION, example = "10000")
    private Long ownerId;

}
