package com.reece.address.book.model;


public enum Role {

    MANAGER("manager"),
    LEAD("lead"),
    CONSULTANT("consultant");

    public final String value;

    Role(String value) {
        this.value = value;
    }
}
