package com.reece.address.book.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import static com.reece.address.book.constants.OpenApiMessageConstants.*;


@Getter
@Setter
public class ContactDTO {

    @Size(max = 255)
    @NotEmpty(message = "Please provide a name")
    @Schema(description = CONTACT_NAME_DESCRIPTION, example = "Oliver Twist")
    private String name;

    @Size(max = 20, min = 8, message = "Please enter phone number")
    @NotEmpty(message = "Please enter phone number")
    @Schema(description = TELEPHONE_DESCRIPTION, example = "0413125654")
    private String phoneNumber;

    @NotNull(message = "Please enter addressBook id")
    @Schema(description = ADDRESS_ID_DESCRIPTION, example = "10001")
    private Long addressBookId;

}
