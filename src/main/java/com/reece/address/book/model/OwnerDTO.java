package com.reece.address.book.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.reece.address.book.validator.EntityEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import static com.reece.address.book.constants.OpenApiMessageConstants.*;


@Getter
@Setter
public class OwnerDTO {

    @NotEmpty(message = "Please provide a name for the owner")
    @Size(max = 255)
    @Schema(description = OWNER_NAME_DESCRIPTION, example = "John Doe")
    private String name;

    @NotNull
    @NotEmpty(message = "Please provide a role")

    @EntityEnum(enumClass = Role.class,
            message = "must have one of these values - manager, lead, consultant")
    @Schema(description = ROLE_DESCRIPTION, example = "manager")
    @Size(max = 255)
    private String role;

}
