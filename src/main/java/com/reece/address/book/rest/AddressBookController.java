package com.reece.address.book.rest;

import com.reece.address.book.model.AddressBookDTO;
import com.reece.address.book.service.AddressBookService;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static com.reece.address.book.constants.OpenApiMessageConstants.CONTACT_CONTROLLER_CREATE_ADDRESS_BOOKS;
import static com.reece.address.book.constants.OpenApiMessageConstants.CONTACT_CONTROLLER_GET_ADDRESS_BOOKS_CONTACTS_FOR_USER;


@RestController
@RequestMapping(value = "/api/books", produces = MediaType.APPLICATION_JSON_VALUE)
public class AddressBookController {

    private final AddressBookService addressBookService;

    public AddressBookController(final AddressBookService addressBookService) {
        this.addressBookService = addressBookService;
    }
    @Hidden
    @GetMapping
    public ResponseEntity<List<AddressBookDTO>> getAllAddressBooks() {
        return ResponseEntity.ok(addressBookService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<AddressBookDTO> getAddressBook(@PathVariable final Long id) {
        return ResponseEntity.ok(addressBookService.get(id));
    }

    @PostMapping
    @Operation(summary = CONTACT_CONTROLLER_CREATE_ADDRESS_BOOKS)
    public ResponseEntity<Long> createAddressBook(
            @RequestBody @Valid final AddressBookDTO addressBookDTO) {
        return new ResponseEntity<>(addressBookService.create(addressBookDTO), HttpStatus.CREATED);
    }

    @Hidden
    @PutMapping("/{id}")
    public ResponseEntity<Void> updateAddressBook(@PathVariable final Long id,
                                                  @RequestBody @Valid final AddressBookDTO addressBookDTO) {
        addressBookService.update(id, addressBookDTO);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteAddressBook(@PathVariable final Long id) {
        addressBookService.delete(id);
        return ResponseEntity.noContent().build();
    }

}
