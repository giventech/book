package com.reece.address.book.rest;

import com.reece.address.book.service.ContactService;
import com.reece.address.book.model.ContactDTO;

import java.util.List;
import javax.validation.Valid;

import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.reece.address.book.constants.OpenApiMessageConstants.*;


@RestController
@RequestMapping(value = "/api/contacts", produces = MediaType.APPLICATION_JSON_VALUE)
public class ContactController {

    private final ContactService contactService;

    public ContactController(final ContactService contactService) {
        this.contactService = contactService;
    }

    @Hidden
    @GetMapping
    public ResponseEntity<List<ContactDTO>> getAllContacts() {
        return ResponseEntity.ok(contactService.findAll());
    }

    @Hidden
    @GetMapping("/owners/{userId}/books/{addressBookIds}")
    @Operation(summary = CONTACT_CONTROLLER_GET_ADDRESS_BOOKS_CONTACTS_FOR_USER)
    public ResponseEntity<List<ContactDTO>> getAllUserBookContactsFromBookIds(@PathVariable final Long userId,@PathVariable final List<Long> addressBookIds) {
        return ResponseEntity.ok(contactService.getAllUserBookContactsFromBookIds(userId,addressBookIds));
    }

    @GetMapping("/books/{addressBookIds}")
    @Operation(summary = CONTACT_CONTROLLER_GET_ADDRESS_BOOKS_CONTACTS)
    public ResponseEntity<List<ContactDTO>> getContactsFromBooks(@PathVariable final List<Long> addressBookIds) {
        return ResponseEntity.ok(contactService.getContactsFromBooks (addressBookIds));
    }


    @GetMapping("/{id}")
    public ResponseEntity<ContactDTO> getContact(@PathVariable final Long id) {
        return ResponseEntity.ok(contactService.get(id));
    }

    @PostMapping("/owners/{ownerId}")
    @Operation(summary = CONTACT_CONTROLLER_CREATE_CONTACT)
    public ResponseEntity<Long> createContact(@PathVariable final Long ownerId, @RequestBody @Valid final ContactDTO contactDTO) {
        return new ResponseEntity<>(contactService.create(ownerId, contactDTO), HttpStatus.CREATED);
    }

    @Hidden
    @PutMapping("/{id}")
    public ResponseEntity<Void> updateContact(@PathVariable final Long id,
            @RequestBody @Valid final ContactDTO contactDTO) {
        contactService.update(id, contactDTO);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @Operation(summary = CONTACT_CONTROLLER_DELETE_CONTACT)
    public ResponseEntity<Void> deleteContact(@PathVariable final Long id) {
        contactService.delete(id);
        return ResponseEntity.noContent().build();
    }

}
