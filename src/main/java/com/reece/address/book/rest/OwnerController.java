package com.reece.address.book.rest;

import com.reece.address.book.service.OwnerService;
import com.reece.address.book.model.OwnerDTO;

import java.util.List;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "/api/owners", produces = MediaType.APPLICATION_JSON_VALUE)
public class OwnerController {

    private final OwnerService ownerService;

    public OwnerController(final OwnerService ownerService) {
        this.ownerService = ownerService;
    }

    @GetMapping
    public ResponseEntity<List<OwnerDTO>> getAllOwners() {
        return ResponseEntity.ok(ownerService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<OwnerDTO> getOwner(@PathVariable final Long id) {
        return ResponseEntity.ok(ownerService.get(id));
    }

    @PostMapping
    public ResponseEntity<Long> createOwner(@RequestBody @Valid final OwnerDTO ownerDTO) {
        return new ResponseEntity<>(ownerService.create(ownerDTO), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> updateOwner(@PathVariable final Long id,
            @RequestBody @Valid final OwnerDTO ownerDTO) {
        ownerService.update(id, ownerDTO);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteOwner(@PathVariable final Long id) {
        ownerService.delete(id);
        return ResponseEntity.noContent().build();
    }

}
