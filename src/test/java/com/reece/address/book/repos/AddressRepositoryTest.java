package com.reece.address.book.repos;

import com.reece.address.book.domain.AddressBook;
import com.reece.address.book.domain.Owner;
import com.reece.address.book.model.Role;
import com.reece.address.book.service.TestUtils;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import static org.junit.Assert.*;

import javax.transaction.Transactional;
import java.util.Optional;

public class AddressRepositoryTest extends BaseIntegrationTest {
    @Autowired
    AddressBookRepository addressBookRepository;

    @Test
    @Transactional
    public void repository_should_find_persisted_address_from_database() {
        Owner owner = new Owner();
        owner.setName("new owner");
        Owner persistedOwner = entityManager.persist(owner);
        Optional<AddressBook> addressBook  = addressBookRepository.findById(persistedOwner.getId());
        assertNotNull(addressBook);
    }

    @Test
    @Transactional
    public void repository_should_create_several_address_book_entry_for_user() {
        //GIVEN
        Owner owner = new Owner();
        owner.setName("new owner");
        owner.setRole(Role.CONSULTANT.name());
        Owner persistedOwner = entityManager.persist(owner);
        int initialCount = addressBookRepository.findAll().size();

        String addressBookName1 = TestUtils.ADDRESS_BOOK_NAME;
        String addressBookName2 = TestUtils.ADDRESS_BOOK_NAME + "2";
        AddressBook addressBook1 = new AddressBook();
        AddressBook addressBook2 = new AddressBook();
        addressBook1.setName(addressBookName1);
        addressBook2.setName(addressBookName2);

        //WHEN
        addressBookRepository.save(addressBook1);
        addressBookRepository.save(addressBook2);

        //THEN

        assertEquals(initialCount+ 2,addressBookRepository.findAll().size());
    }
}
