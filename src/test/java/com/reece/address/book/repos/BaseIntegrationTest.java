package com.reece.address.book.repos;


import com.reece.address.book.domain.Owner;
import org.junit.jupiter.api.Test;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

import static org.junit.Assert.*;
@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class BaseIntegrationTest {
        @Autowired
        protected TestEntityManager entityManager;

        @Autowired
        AddressBookRepository addressBookRepository;

        @Test
        public void repository_should_do_do_nothing_as_required_on_parent_unit_test_class() {
                assertTrue(true);
        }

        @Test
        @Transactional
        public void repository_should_find_persisted_address_from_database() {
                Owner owner = new Owner();
                owner.setName("new owner");
                Owner persistedOwner = entityManager.persist(owner);
                addressBookRepository.findById(persistedOwner.getId());

                //
        }

    }

