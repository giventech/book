package com.reece.address.book.controller;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.reece.address.book.BookApplication;
import com.reece.address.book.builder.ContactDTOBuilder;
import com.reece.address.book.model.ContactDTO;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = BookApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ContactControllerIntegrationTest {
    TestRestTemplate restTemplate = new TestRestTemplate();
    HttpHeaders headers = new HttpHeaders();
    @LocalServerPort
    private int port;


    @Test
    public void rest_call_should_create_a_contact_successfully() {
        ContactDTO contact = new ContactDTOBuilder()
                .withName("John")
                .withPhoneNumber("0413123456")
                .withAddressBookId(Long.parseLong("1005"))
                .build();
        HttpEntity<ContactDTO> entity = new HttpEntity<ContactDTO>(contact, headers);
        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort("/contacts/owners/1004"),
                HttpMethod.POST, entity, String.class);
        assertEquals(HttpStatus.CREATED,  response.getStatusCode());
    }


    @Test
    public void rest_call_should_get_one_contact_in_address_book() {
         HttpEntity<?> entity = new HttpEntity<ContactDTO>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort("/contacts/books"+"/1005"),
                HttpMethod.GET, entity, String.class);
        assertEquals(HttpStatus.OK,  response.getStatusCode());
    }


    @Test
    public void rest_call_should_get_contacts_accross_several_address_books() {
        HttpEntity<?> entity = new HttpEntity<ContactDTO>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort("/contacts/books"+"/1001,1005"),
                HttpMethod.GET, entity, String.class);
        DocumentContext pathJSonResult   =  JsonPath.parse(response.getBody());
        int size = pathJSonResult.read("$.length()");
        List<Integer> ids = new ArrayList<>();
        for (int i =0; i <size; i++ ) {
            ids.add(pathJSonResult.read("$.[" + i + "].addressBookId"));
        }
        boolean foundIds = ids.stream().anyMatch(a -> a.equals(Integer.parseInt("1001"))) &&
                ids.stream().anyMatch(a -> a.equals(Integer.parseInt("1005")));
        assertEquals(HttpStatus.OK,  response.getStatusCode());
        assertTrue(foundIds);
    }

    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + "/api" + uri;
    }
}