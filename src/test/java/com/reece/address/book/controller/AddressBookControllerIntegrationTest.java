package com.reece.address.book.controller;

import com.reece.address.book.BookApplication;
import com.reece.address.book.builder.AddressDTOBuilder;
import com.reece.address.book.model.AddressBookDTO;
import com.reece.address.book.service.TestUtils;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = BookApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AddressBookControllerIntegrationTest {
    TestRestTemplate restTemplate = new TestRestTemplate();
    HttpHeaders headers = new HttpHeaders();
    @LocalServerPort
    private int port;

    @Test
    public void rest_calls_should_create_two_address_book_for_a_given_owner() {
        Long testOwnerId = Long.parseLong("1020");
        //Given
        AddressBookDTO addressBookDTO = new AddressDTOBuilder()
                .withName(TestUtils.ADDRESS_BOOK_NAME)
                .withOwnerId(testOwnerId)
                .build();
        AddressBookDTO addressBookDTO2 = new AddressDTOBuilder()
                .withName(TestUtils.ADDRESS_BOOK_NAME + "2")
                .withOwnerId(testOwnerId)
                .build();
        HttpEntity<AddressBookDTO> addressBookDTOHttpEntity = new HttpEntity<AddressBookDTO>(addressBookDTO, headers);
        HttpEntity<AddressBookDTO> addressBookDTOHttpEntity1 = new HttpEntity<AddressBookDTO>(addressBookDTO2, headers);

        //When
        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort("/books"),
                HttpMethod.POST, addressBookDTOHttpEntity, String.class);
        ResponseEntity<String> response2 = restTemplate.exchange(
                createURLWithPort("/books"),
                HttpMethod.POST, addressBookDTOHttpEntity1, String.class);
        //Then
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(HttpStatus.CREATED, response2.getStatusCode());
    }
    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + "/api" + uri;
    }
}