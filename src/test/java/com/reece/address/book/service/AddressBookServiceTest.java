package com.reece.address.book.service;


import com.reece.address.book.domain.AddressBook;
import com.reece.address.book.domain.Contact;
import com.reece.address.book.model.AddressBookDTO;
import com.reece.address.book.model.ContactDTO;
import com.reece.address.book.repos.AddressBookRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class AddressBookServiceTest {


    // Service being tested
    @InjectMocks
    private AddressBookService addressBookService;

    @Mock
    private AddressBookRepository addressBookRepository;

    @BeforeEach
    void before() {
        MockitoAnnotations.openMocks(this);

    }

    @Test
    void service_should_call_repository_once_when_validation_are_sucessful() {
        AddressBook book = new AddressBook().setName("new Address book");
        book.setId(TestUtils.ADDRESS_BOOK_ID);
        book.setName(TestUtils.ADDRESS_BOOK_NAME);
        when(addressBookRepository.findById(any())).thenReturn(Optional.of(book));
        AddressBookDTO bookDTO = addressBookService.get(TestUtils.ADDRESS_BOOK_ID);
        assertEquals(TestUtils.ADDRESS_BOOK_NAME, bookDTO.getName());
    }









}
