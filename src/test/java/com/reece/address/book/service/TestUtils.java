package com.reece.address.book.service;


import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class TestUtils {

    final public static Long ADDRESS_BOOK_ID = new Random().nextLong();
    final public static Long CONTACT_ID = new Random().nextLong();
    final public static Long OWNER_ID = new Random().nextLong();

    final public static String ADDRESS_BOOK_NAME = "ANY NAME";


    public static final String CONTACT_NAME = "ANY CONTACT NAME";
    public static final String OTHER_CONTACT_NAME = "ANY OTHER CONTACT NAME";
    public static final String OWNER_NAME = "AN OWNER NAME" ;
}
