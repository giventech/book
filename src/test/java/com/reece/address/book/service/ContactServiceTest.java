package com.reece.address.book.service;


import com.reece.address.book.builder.AddressDTOBuilder;
import com.reece.address.book.builder.ContactDTOBuilder;
import com.reece.address.book.domain.AddressBook;
import com.reece.address.book.domain.Contact;
import com.reece.address.book.domain.Owner;
import com.reece.address.book.model.AddressBookDTO;
import com.reece.address.book.model.ContactDTO;
import com.reece.address.book.repos.AddressBookRepository;
import com.reece.address.book.repos.ContactRepository;
import com.reece.address.book.repos.OwnerRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class ContactServiceTest {


    // Service being tested
    @InjectMocks
    private ContactService contactService;

    @Mock
    private OwnerRepository ownerRepository;

    @Mock
    private ContactRepository contactRepository;

    @Mock
    private AddressBookRepository addressBookRepository;

    @BeforeEach
    void before() {
        MockitoAnnotations.openMocks(this);

    }

    @Test
    void service_should_create_contact_entry_for_address_owner() {
        //GIVEN owner and address book
        Long ownerId = TestUtils.OWNER_ID;
        Owner owner = new Owner();
        owner.setName("new owner");
        ContactDTO contactDTO = new ContactDTOBuilder().withName(TestUtils.CONTACT_NAME).build();
        Contact contact1 = new Contact();
        contact1.setId(TestUtils.CONTACT_ID);
        AddressBookDTO bookDTO = new AddressDTOBuilder().withName(TestUtils.ADDRESS_BOOK_NAME).build();
        AddressBook addressBook = new AddressBook();
        addressBook.setName(TestUtils.ADDRESS_BOOK_NAME);
        addressBook.setId(TestUtils.ADDRESS_BOOK_ID);

        //When I create a contact
        when(ownerRepository.findById(any())).thenReturn(Optional.of(owner));
        when(addressBookRepository.findAddressBookByOwnerAndId(any(), any())).thenReturn((Optional.of(addressBook)));
        when(contactRepository.save(any())).thenReturn(contact1);


        contactService.create(ownerId,contactDTO);
        //THEN
        verify(ownerRepository, times(1)).findById(any());
        verify(addressBookRepository, times(1)).findAddressBookByOwnerAndId(any(), any());

        //assertEquals(TestUtils.ADDRESS_BOOK_ID, bookDTO.getId());
    }

    @Test
    void service_should_delete_contact_entry() {
        //GIVEN owner and address book

        contactService.delete(TestUtils.CONTACT_ID);
        //THEN
        verify(contactRepository, times(1)).deleteById(any());
    }

    @Test
    void service_should_return_all_contacts_from_an_address_book() {
        //GIVEN
        Long ownerId = TestUtils.OWNER_ID;
        Long addressId = TestUtils.ADDRESS_BOOK_ID;
        Contact contact1 = new Contact();
        contact1.setName(TestUtils.CONTACT_NAME);
        Contact contact2 = new Contact();
        contact1.setName(TestUtils.OTHER_CONTACT_NAME);
        List<Contact> contacts = Arrays.asList(contact1, contact2);
        List <Long> contactIds = contacts.stream().map(contact -> contact.getId()).collect(Collectors.toList());
        when(contactRepository.fetchContactsByAddressBooks(any())).thenReturn(contacts);
        //WHEN
        List<ContactDTO> returnedContacts = contactService.getContactsFromBooks(contactIds);

        //THEN
        verify(contactRepository, times(1)).fetchContactsByAddressBooks(contactIds);
        assertNotNull(returnedContacts);
        assertTrue(returnedContacts.size() > 0);
        assertEquals(2, returnedContacts.size());
    }


    @Test
    void service_should_return_all_contacts_from_a_user_address_book() {
        //GIVEN
        Long ownerId = TestUtils.OWNER_ID;
        Long addressId = TestUtils.ADDRESS_BOOK_ID;
        Contact contact1 = new Contact();
        contact1.setName(TestUtils.CONTACT_NAME);
        Contact contact2 = new Contact();
        contact1.setName(TestUtils.OTHER_CONTACT_NAME);
        List<Contact> contacts = Arrays.asList(contact1, contact2);
        when(contactRepository.fetchContactsByAddressContactIdAndOwnerId(any(), any())).thenReturn(contacts);
        //WHEN
        List<ContactDTO> returnedContacts = contactService.getContactsByUserAddressBook(ownerId, addressId);

        //THEN
        verify(contactRepository, times(1)).fetchContactsByAddressContactIdAndOwnerId(addressId, ownerId);
        assertNotNull(returnedContacts);
        assertTrue(returnedContacts.size() > 0);
        assertEquals(2, returnedContacts.size());
    }



}
