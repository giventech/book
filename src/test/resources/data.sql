-- Index 1-9999 are reserved to create test data

insert into owner (id, date_created, last_updated, name, role) values (1000, '2021-05-18 12:41:34.009465', '2021-05-18 12:41:34.009465', 'George', 'Manager');
insert into owner (id, date_created, last_updated, name, role) values (1004, '2021-05-19 11:12:46.026607', '2021-05-19 11:12:46.026607', 'Paul', 'Manager');
insert into owner (id, date_created, last_updated, name, role) values (1019, '2021-05-19 09:32:57.020133', '2021-05-19 09:32:57.020133', 'Mark ', 'Manager');
INSERT INTO address_book (id, date_created, last_updated, owner_id, name) VALUES (1001, '2021-05-18 12:42:07.101943', '2021-05-18 12:42:07.101943',  1000, 'BOOK0');
INSERT INTO address_book (id, date_created, last_updated, owner_id, name) VALUES (1005, '2021-05-19 11:15:49.562144', '2021-05-19 11:15:49.562144',  1004, 'BOOK1');

INSERT INTO contact (id,date_created, last_updated, name, phone_number, address_contact_id) VALUES (1001,'2021-05-19 12:01:20.521802', '2021-05-19 12:01:20.521802', 'CONTACT A', 'string', 1005);
INSERT INTO contact (id, date_created, last_updated, name, phone_number, address_contact_id) VALUES (1002,'2021-05-19 12:04:51.564576', '2021-05-19 12:04:51.564576', 'CONTACT B', '0413157', 1005);
INSERT INTO contact (id, date_created, last_updated, name, phone_number, address_contact_id) VALUES (1003,'2021-05-19 12:04:51.564576', '2021-05-19 12:04:51.564576', 'CONTACT C', '0413157', 1001);




-- Init with owners

insert into owner (id, date_created, last_updated, name, role) values (1020, '2021-05-18 12:41:34.009465', '2021-05-18 12:41:34.009465', 'George', 'Manager');
insert into owner (id, date_created, last_updated, name, role) values (1021, '2021-05-19 11:12:46.026607', '2021-05-19 11:12:46.026607', 'Paul', 'Manager');
insert into owner (id, date_created, last_updated, name, role) values (1022, '2021-05-19 09:32:57.020133', '2021-05-19 09:32:57.020133', 'Mark ', 'Manager');


