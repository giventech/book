ARG REPOSITORY_URL

FROM ${REPOSITORY_URL}/openjdk:11-jdk-slim as builder

WORKDIR application
ARG JAR_FILE=build/libs/*.jar
COPY ${JAR_FILE} book-0.0.1-SNAPSHOT.jar
RUN java -Djarmode=layertools -jar book-0.0.1-SNAPSHOT.jar extract

ARG REPOSITORY_URL

FROM ${REPOSITORY_URL}/openjdk:11-jdk-slim
WORKDIR application
COPY --from=builder application/dependencies/ ./
COPY --from=builder application/snapshot-dependencies/ ./
COPY --from=builder application/spring-boot-loader/ ./
COPY --from=builder application/application/ ./

COPY ./entrypoint.sh  /
RUN chmod +x /entrypoint.sh

ARG SPRING_PROFILE
ENV SPRING_PROFILES_ACTIVE=${SPRING_PROFILE}

EXPOSE 8900
ENTRYPOINT ["/entrypoint.sh"]
