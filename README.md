# Spring Boot REST-API based micro-service managing contact and addresses

### Table of content

- [Overview](##Overview)
- [IDE setup](#IDE setup)
- [How to build the project](#How to build the project)
- [How to run the microservices](#How to run the microservices)
- [How to run using Docker](## How to build/run with docker/docker-compose)
- [How to run tests](#How to run tests)
- [How to update Swagger documentation](#How to update Swagger documentation)
- [Kubernetes local deploy](#Kubernetes local deploy)

## Overview
This documentation outlines the project structure, dependencies/plugins used and guidelines for further development.
Address book holds name and phone numbers of contact entries REST API will have the endpoints to fulfill the below

- User is able to add new contact entry
- User is able to remove existing contact entries
- User should be able to print all contacts in an address book.
- User should be able to maintain multiple address books

## Pre-requisite
- [Minimum Gradle 6.7.1](https://docs.gradle.org/6.7.1/release-notes.html)
- [JDK 11 LTS](https://adoptopenjdk.net/installation.html#installers)
- [Docker desktop](https://hub.docker.com/editions/community/docker-ce-desktop-windows)


## IDE setup
This project has been build using IntelliJ Idea. Following plugins should be installed in the IntelliJ, in order to build and perform various checks :


## Development strategy ( with Agile & DevOps for cloud native application in mind)

Principle applied for design

- Domain Driven Design - (Domain is contact with the vision of it becoming a micro-service)
- S.O.L.I.D. & Separation of concern (Entity, Repositories, Service and RestControllers, BuilderPattern)
- DRY principle whenever possible

External configuration
- default (local) and dev (for docker) environment are created via spring profiles

Test Driven Development / Integration testing
 
- H2 database is used for local development (satisfying DB and integration testing requirements)
- Dockerised postgresql db + spring boot is used when building application as containerised app  
- Unit (with Mock) and Integration testing (Repository) and REST-API (controller) ensure code quality as it is refactored

Domain model

![Domain model](docs/domainmodel.png)

Test folder

![Test Folder](docs/Test.jpg)


## Toolset

### Lombok
This project uses Lombok to generate boilerplate code, such as getters, setters and default constructors.

### SonarLint
Sonarlint plugin is used to analyze the code, catch code smells and help maintain a good codebase.

### OpenAPI specs - documentation 
A code first approach is chosen to  generat the REST API documentation, specification and help other people to test the API.
API Documentation and contract first approach should be undertaken when API must be consumed by several consumers 

## How to build the project
Clone repository as below:.

    git clone https://gitlab.com/giventech/book

Run the following command at the root (book) folder :
    
    gradle clean build
    
## How to run 

As of now, we can build and run the jar on local directly
    
    java -jar book/build/libs/book-0.0.1-SNAPSHOT.jar

Alternatively 

    grable bootRun
    
The API documentation can  be accessed using Swagger at this URL :

        
    UI - http://localhost:8080/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config
    JSON - http://localhost:8080/v3/api-docs/
    YAML - http://localhost:8080/v3/api-docs.yaml

 
 The application utilises `org.springdoc:springdoc-openapi-ui` to perform code first API definition/documentation
 After running the application locally, the OpenApi documentation can be accessed on below URL's
    
    
    
## How to test    
Automated :

     grable test
       
Manually :  (Either from swagger ui or curl following the below steps)

1. Start the application
    
(refer ## How to run section)
   

2. Create an owner 

    `curl -X POST "http://localhost:8080/api/owners" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"name\":\"John Doewerw\",\"role\":\"manager\"}"`

3. Create one or more  address book for this owner 

    `curl -X POST "http://localhost:8080/api/books" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"name\":\"Work related address book\",\"ownerId\":10000}"`

    `curl -X POST "http://localhost:8080/api/books" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"name\":\"Partner agencies related address book\",\"ownerId\":10000}"`
    
    
4. Create one or more contacts in the owners' address books

    `curl -X POST "http://localhost:8080/api/contacts/owners/10000" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"name\":\"Oliver Twist\",\"phoneNumber\":\"0413125654\",\"addressBookId\":10001}"`
    
    `curl -X POST "http://localhost:8080/api/contacts/owners/10000" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"name\":\"Nelson Mandela\",\"phoneNumber\":\"0413125654\",\"addressBookId\":10002}"`
    
    `curl -X POST "http://localhost:8080/api/contacts/owners/10000" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"name\":\"Sean Connery\",\"phoneNumber\":\"0413125654\",\"addressBookId\":10001}"`

5. Fetch contact for one or more address books 

    `curl -X GET "http://localhost:8080/api/contacts/books/10001" -H  "accept: application/json"`
    
    `curl -X GET "http://localhost:8080/api/contacts/books/10002,10001" -H  "accept: application/json"`
    
    
6. Expected results

    ![Search on one address book](docs/searchresults1.jpg)
    
    ![Search on two address books](docs/searchresults2.jpg)
## How to build/run with docker/docker-compose

The project contains 2 files :

- `Dockerfile` that contains the script to build the docker image.
- `docker-compose.yml` that will both build and deploy the API in a docker on local machine.

Note: the Docker image uses cloud native pack for spring boot 2.3.+ to optimise layers size and build time extracting fat jar into dedicated layers
Building fat jar must be done prior to `docker build`. (Typically as part of a pipeline, noting that  installing /performing gradle build from Dockerfile docker images is counter performant as dependent jar would be downloaded each time a image is build)  

To build manually perform the below: 
1.  Stop any running application on port `8080`
1. `gradle clean build` (this build the project and fat jar)
2. `docker-compose build --build-arg REPOSITORY_URL=docker.io --build-arg SPRING_PROFILE=dev` (build spring boot image`book-api:latest` based on `Dockerfile`)
3. `docker-compose up` ( first build/run Postgresql image and container, then build/run `book-api:latest` )

You can then check for the docker images

    >> docker images
    
You can also find the running container

    >> docker ps
    
* Note use the "services" feature in IntelliJ to view, can be managed to monitor docker artifacts.



## List of Improvements

- [x] Integration tests - with spring boot tests
- [x] Initial dataset on launch
- [ ] Kubernetes local deploy with ingress & side car
- [ ] Flyway + Test containers / Liquidbase database versioning
- [ ] JWT Authentication

## Kubernetes local deploy (IN Progress)

Install a local Kubernetes Cluster (with a single node) on your machine:

* For Linux: [MicroK8s](https://microk8s.io/docs/)
* For Mac/Windows: A local Kubernetes Cluster can be enabled with the latest versions of Docker for Windows/Mac

Install nginx Ingress

`kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-0.32.0/deploy/static/provider/cloud/deploy.yaml`

Set repository environment variable

{REPOSITORY_URL} is the name of the repository where images arepulled from
e.g. docker.io or <internal>.<client>.<repository>

1. Start your Docker daemon and ensure your local Kubernetes Cluster is up- and running
2. Create a local registry with `docker run -d -p 5000:5000 --restart=always --name registry registry:2`
3. Build the application with gradle instruction 
4. Create the Docker image for this application `docker build -t book-api --build-arg REPOSITORY_URL=docker.io .`
5. Tag the Docker image (to be able to push it to the local registry) `docker tag book-api localhost:5000/book-api`
6. Push the Docker image to the local registry `docker push localhost:5000/book-api`
7. Execute `kubectl apply -f book-api.yml` to deploy the application to Kubernetes


#Basic debugging:
`kubectl get pods -o wide`
1. Debugging running pod. `kubectl logs <podname>`

or for a Pod with multiple pod: `kubectl logs ${POD_NAME} ${CONTAINER_NAME}`

2. Debugging crashed pod. `kubectl logs <podname> --previous`

or for a Pod with multiple containers: `kubectl logs --previous ${POD_NAME} ${CONTAINER_NAME}`

